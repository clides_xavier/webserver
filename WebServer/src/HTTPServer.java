import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HTTPServer {

    ServerSocket serverSocket;

    private int portNumber = 8085;

    public HTTPServer(){
        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void startServer(){
        try {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            byte[] bytes = Files.readAllBytes(Paths.get("www/THeFile.html")) ;

            String header = "HTTP/1.0 200 Document Follows\r\n" +
                             "Content-Type: text/html; charset=UTF-8\r\n" +
                             "Content-Length: <file_byte_size> \r\n" +
                             "\r\n";

            out.write(header.getBytes());
            out.write(bytes);
            out.flush();







        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




}
